import matplotlib.pyplot as plt
import seaborn as sns
from statsmodels.graphics.mosaicplot import mosaic
import pandas as pd
from xpresso.ai.core.data.visualization.seaborn.res.plot_factory import Plot
from xpresso.ai.core.data.visualization import utils
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InputLengthMismatch, AttributeNotPresent


class NewPlot(Plot):
    """
    Generates a mosaic plot
    Attributes:
        plot(:obj: matplotlib.axes): Matplotlib axes which stores the plot
    Args:
        input_1(list): X axis data
        input_2(list): Y axis data
        plot_title(str): Title to the plot
        output_format (str): html or png plots to be generated
        file_name(str): File name of the plot to be stored
        axes_labels(dict): Dictionary of x_label, y_label and
            target_label
        output_path(str): path where the html/png plots to be stored
    """
    def __init__(self, input_1, input_2=None, plot_title=None,
                 output_format=utils.HTML,
                 file_name=None, axes_labels=None,
                 output_path=utils.DEFAULT_IMAGE_PATH, columns=None):
        super().__init__()
        sns.set(font_scale=1.5)
        if axes_labels:
            self.axes_labels = axes_labels
        if isinstance(input_1, pd.DataFrame):
            try:
                input_1 = input_1[columns[0]]
                input_2 = input_1[columns[1]]
            except KeyError:
                raise AttributeNotPresent
        if len(input_1) != len(input_2):
            raise InputLengthMismatch
        self.figure, self.plot = plt.subplots(figsize=self.figure_size)
        self.plot.set(xlabel=self.axes_labels[utils.X_LABEL],
                      ylabel=self.axes_labels[utils.Y_LABEL])
        data = pd.DataFrame(list(zip(input_1, input_2)), columns=[0, 1])
        mosaic(data, [0, 1], labelizer=lambda x: "", ax=self.plot,
               title=plot_title)

        self.render(output_format=output_format, output_path=output_path,
                    file_name=file_name)
