""" Utility modules"""

from pyspark.mllib.stat import Statistics
from pyspark.mllib.util import MLUtils


def convert_feature_to_rdd(df):
    """ Convert features column to an RDD of vectors. """
    features = MLUtils.convertVectorColumnsFromML(df, "features") \
        .select("features").rdd.map(lambda r: r.features)
    summary = Statistics.colStats(features)
    print("Selected features column with average values:\n" +
          str(summary.mean()))
    return df